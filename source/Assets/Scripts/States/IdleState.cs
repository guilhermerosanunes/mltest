﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : State<Enemy>
{
    public override void EnterState(Enemy owner)
    {
        owner.GetComponent<Collider>().enabled = true;
        owner.animator.SetBool("Idle", true);
    }

    public override void ExitState(Enemy owner)
    {
        owner.animator.SetBool("Idle", false);
    }

    public override void UpdateState(Enemy owner)
    {
        if (owner.distanceToPlayer < owner.distanceToPursue )
            owner.stateMachine.ChangeState(new PursueState());
    }
}
