﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine<T>
{
    public State<T> currentState { get; private set; }
    public T owner;

    protected bool transitioning;

    public StateMachine(T _owner)
    {
        owner = _owner;
        currentState = null;
    }

    public void Update()
    {
        if (currentState != null)
        {
            currentState.UpdateState(owner);
        }
    }


    public void ChangeState(State<T> newState)
    {
        if (currentState != null)
            currentState.ExitState(owner);

        currentState = newState;
        currentState.EnterState(owner);
    }
}
