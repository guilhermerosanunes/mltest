﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public static EnemySpawner Instance;
    public GameObject warriorPrefab;
    public GameObject archerPrefab;

    public float warriorSpawnRate;
    public float archerSpawnRate;

    public int poolAmount;

    private List<GameObject> warriors = new List<GameObject>();
    private List<GameObject> archers = new List<GameObject>();

    private float warriorCurrentTime;
    private float archerCurrentTime;

    //map bounds
    public Transform xMin;
    public Transform zMax;

    //used to randomize enemy spawn position
    private float xmin, xmax, zmin, zmax;

    private void Awake()
    {
        if (Instance != null)
            Destroy(gameObject);

        Instance = this;
    }

    private void Start()
    {
        xmin = xMin.position.x;
        zmin = xMin.position.z;

        xmax = zMax.position.x;
        zmax = zMax.position.z;

        //pool enemies
        for (int i = 0; i < poolAmount; i++)
        {
            GameObject warrior = Instantiate(warriorPrefab);
            GameObject archer = Instantiate(archerPrefab);

            warrior.SetActive(false);
            archer.SetActive(false);

            warriors.Add(warrior);
            archers.Add(archer);
        }

        warriorCurrentTime = warriorSpawnRate;
        archerCurrentTime = archerSpawnRate;
    }

    private void Update()
    {
        if (GameManager.Instance.state != GameState.Playing)
            return;

        warriorCurrentTime -= Time.deltaTime;
        archerCurrentTime -= Time.deltaTime;

        if (warriorCurrentTime <= 0f)
            SpawnWarrior();

        if (archerCurrentTime <= 0f)
            SpawnArcher();

    }

    private void SpawnWarrior()
    {
        Vector3 position = GetRamdomPosition();

        for (int i = 0; i < warriors.Count; i++)
        {
            if (!warriors[i].activeInHierarchy)
            {
                warriors[i].transform.position = position;
                warriors[i].SetActive(true);
                break;
            }
        }

        warriorCurrentTime = warriorSpawnRate;
    }

    private void SpawnArcher()
    {
        Vector3 position = GetRamdomPosition();

        for (int i = 0; i < archers.Count; i++)
        {
            if (!archers[i].activeInHierarchy)
            {
                archers[i].transform.position = position;
                archers[i].SetActive(true);
                break;
            }
        }

        archerCurrentTime = archerSpawnRate;
    }

    private Vector3 GetRamdomPosition()
    {
        float x = Random.Range(xmin, xmax);
        float y = 1f;
        float z = Random.Range(zmin, zmax);

        return new Vector3(x, y, z);
    }

}
