﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public int attackValue;
    public int lifeValue;

    public Text uiLifeValue;

    public bool isDead { get; private set; }

    private void Start()
    {
        uiLifeValue.text = lifeValue.ToString();
    }

    public void TakeDamage(int enemyAttack)
    {
        lifeValue -= enemyAttack;
        lifeValue = Mathf.Max(0, lifeValue);

        if (lifeValue == 0)
        {
            isDead = true;
            GameManager.Instance.GameOver();
        }

        uiLifeValue.text = lifeValue.ToString();
    }

    public void TakeHeal(int healValue)
    {
        lifeValue += healValue;

        lifeValue = Mathf.Min(lifeValue, 100);

        uiLifeValue.text = lifeValue.ToString();
    }

    public void OnCollisionEnter(Collision other)
    {
        GameObject obj = other.gameObject;

        if (obj.tag == "Item")
        {
            ItemController item = obj.GetComponentInParent<ItemController>();

            switch (item.type)
            {
                case ItemType.Medkit:
                    int healValue = (item as Medkit).healValue;
                    TakeHeal(healValue);
                    break;

                case ItemType.Ammo:
                    int quantity = (item as Quiver).quantity;
                    GetComponent<WeaponController>().AddAmmo(quantity);
                    break;
            }

            item.gameObject.SetActive(false);
        }

    }

}
