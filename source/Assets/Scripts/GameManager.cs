﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum GameState { NotStarted, Playing, Paused, Fineshed} 

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public GameObject player;

    public GameState state { get; private set; }

    public int gameTimeInSeconds { get; private set; }
    public float countdownTime;
    public int enemiesDead { get; private set; }
    public int points { get; private set; }

    public Text gameTime;
    public Text enemiesDeadCount;
    public Text pointsValue;
    public Text countdown;
    public GameObject finalUIPanel;
    public GameObject pausedUIPanel;

    public Text finalPoints;
    public Text finalTime;
    public Text finalDeaths;

    public Button restartButton;
    public Button menuButton;
    public Button resumeButton;

    private int min;
    private float secs;

    private AudioSource audioSource;

    private List<GameObject> enemies = new List<GameObject>();

    private void Awake()
    {
        if (Instance != null)
            Destroy(gameObject);

        Instance = this;
    }

    private void Start()
    {
        if (PlayerPrefs.HasKey("config"))
        {
            Camera.main.GetComponent<AudioListener>().enabled = PlayerPrefs.GetInt("music") != 0;
            gameTimeInSeconds = PlayerPrefs.GetInt("time");
            player.GetComponent<WeaponController>().SetInitialAmmo(PlayerPrefs.GetInt("arrows"));
        }
        else
        {
            Camera.main.GetComponent<AudioListener>().enabled = true; //enable music
            gameTimeInSeconds = 180;
            player.GetComponent<WeaponController>().SetInitialAmmo(60);

        }

        audioSource = GetComponent<AudioSource>();

        gameTime.text = getInitialTime();
        state = GameState.NotStarted;
        countdown.text = countdownTime.ToString();

        restartButton.onClick.AddListener(RestartScene);
        menuButton.onClick.AddListener(LoadPreviousScene);
        resumeButton.onClick.AddListener(ResumeGame);
    }

    private void Update()
    {
        switch (state)
        {
            case GameState.NotStarted:
                UpdateCountdown();
                break;

            case GameState.Playing:
                if (!audioSource.isPlaying)
                    audioSource.Play();

                UpdateGameTimer();

                if (Input.GetKeyUp(KeyCode.Escape))
                    PauseGame();
                break;

            case GameState.Paused:
                if (Input.GetKeyUp(KeyCode.Escape))
                    ResumeGame();
                break;

            case GameState.Fineshed:
                break;
        }
    }


    #region Helpers

    private void PauseGame()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        audioSource.Pause();
        Time.timeScale = 0f;
        pausedUIPanel.SetActive(true);
        state = GameState.Paused;
    }

    private void ResumeGame()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        audioSource.Play();
        Time.timeScale = 1f;
        pausedUIPanel.SetActive(false);
        state = GameState.Playing;
    }

    private void UpdateCountdown()
    {
        countdownTime -= Time.deltaTime;
        countdown.text = ((int)countdownTime).ToString();

        if (countdownTime < 0f)
        {
            countdown.gameObject.SetActive(false);
            state = GameState.Playing;
        }
    }

    private string getInitialTime()
    {
        min = gameTimeInSeconds / 60;
        secs = gameTimeInSeconds % 60;

        if(secs < 10)
            return min.ToString() + ":0" + secs.ToString();
        else
            return min.ToString() + ":" + secs.ToString();
    }

    public void AddPoints(int points)
    {
        int current = int.Parse(pointsValue.text) + points;
        pointsValue.text = current.ToString();
    }

    private void UpdateGameTimer()
    {
        secs -= Time.deltaTime;
        bool isOver = false;
        if ((int)secs == 0)
        {
            min--;
            secs = 59;

            if (min == -1)
            {
                min = 0;
                secs = 0;

                //GameOver();
                isOver = true;
            }
        }

        if (secs < 10)
            gameTime.text = min.ToString() + ":0" + ((int) secs).ToString();
        else gameTime.text = min.ToString() + ":" + ((int) secs).ToString();

        if (isOver)
            GameOver();
    }

    public void GameOver()
    {
        finalUIPanel.SetActive(true);

        finalDeaths.text = enemiesDead.ToString();
        finalPoints.text = pointsValue.text;
        finalTime.text = gameTime.text;

        state = GameState.Fineshed;

        Time.timeScale = 0;

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void CauseDamageToPlayer(int attack)
    {
        player.GetComponent<PlayerController>().TakeDamage(attack);
    }

    public void RestartScene()
    {
        if (Time.timeScale != 1) Time.timeScale = 1f;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void LoadPreviousScene()
    {
        if (Time.timeScale != 1) Time.timeScale = 1f;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    public void AddDeadEnemy()
    {
        enemiesDead++;
        enemiesDeadCount.text = enemiesDead.ToString();
    }

    #endregion

}
