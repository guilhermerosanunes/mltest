﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum Scenes { Menu, Game }

//responsable for scene changing 
public class ApplicationController : MonoBehaviour
{
    public static ApplicationController Instance;

    public Scenes currentScene { get; private set; }

    private void Awake()
    {
        if (Instance != null)
            Destroy(gameObject);

        Instance = this;
        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        currentScene = (Scenes) SceneManager.GetActiveScene().buildIndex;
    }

    public void LoadNextScene()
    {
        currentScene++;
        StartCoroutine(LoadScene());
    }

    public void LoadPreviousScene()
    {
        currentScene--;
        StartCoroutine(LoadScene());
    }

    public void RestartScene()
    {
        //StartCoroutine(LoadScene());
        SceneManager.LoadScene((int)currentScene);
    }

    private IEnumerator LoadScene()
    {
        yield return new WaitForSeconds(1f);

        SceneManager.LoadSceneAsync((int)currentScene);
    }

    public void Quit()
    {
        Application.Quit();
    }

}
