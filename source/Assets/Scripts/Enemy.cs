﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class Enemy : MonoBehaviour
{
    public int life;
    public int attackValue;
    public int pointsValue;
    public float distanceToPursue;
    public float distanceToAttack;

    [Range(0, 100)]
    public int dropItemChance;

    public RectTransform uiRectLife;

    //state machine controls enemies actions;
    public StateMachine<Enemy> stateMachine = null;
    public Animator animator { get; private set; }
    public NavMeshAgent navMeshAgent { get; private set; }

    public float distanceToPlayer { get; private set; }
    public bool isDead { get; private set; }
    private Transform playerTransform;

    private int originalLife;
    public Canvas canvas { get; private set; }

    private void Start()
    {
          
    }

    protected virtual void OnEnable ()
    {
        if(stateMachine == null)
            stateMachine = new StateMachine<Enemy>(this);

        if(animator == null)
            animator = GetComponentInChildren<Animator>();

        if (navMeshAgent == null)
        {
            navMeshAgent = GetComponentInChildren<NavMeshAgent>();
            navMeshAgent.stoppingDistance = distanceToAttack;
        }

        playerTransform = GameManager.Instance.player.transform;
        canvas = uiRectLife.GetComponentInParent<Canvas>();
        originalLife = life;

    }

    private void OnDisable()
    {
        life = originalLife;
        uiRectLife.localScale = new Vector3(1f, uiRectLife.localScale.y, uiRectLife.localScale.z);
        isDead = false;
    }

    protected virtual void Update()
    {
        if (isDead)
            return;

        distanceToPlayer = Vector3.Distance(transform.position, playerTransform.position);
        canvas.gameObject.transform.LookAt(new Vector3(playerTransform.position.x, uiRectLife.transform.position.y, playerTransform.position.z));

        
        stateMachine.Update();
    }

    public void TestDamageToPlayer()
    {
        //verify if the player is near and hit him
        if (distanceToPlayer < distanceToAttack)
        {
            GameManager.Instance.CauseDamageToPlayer(attackValue);
        }
    }

    public void TakeDamage(int playerAttack)
    {
        life -= playerAttack;
        life = Mathf.Max(0, life);

        float newLife = (life / (float)originalLife);
        uiRectLife.localScale = new Vector3(newLife, uiRectLife.localScale.y, uiRectLife.localScale.z);

        if (life == 0)
        {
            isDead = true;
            stateMachine.ChangeState(new DeathState());
            GameManager.Instance.AddPoints(pointsValue);
        }
    }

    public void TryDropItem()
    {
        int chance = Random.Range(0, 101);
        bool drop =  (chance <= dropItemChance);

        if (drop)
        {
            int i = Random.Range(0, 2);

            if (i == 0)
                ItemSpawner.Instance.CreateMedkitAt(transform.position);
            else
                ItemSpawner.Instance.CreateQuiverAt(transform.position);
        }
    }

    public void LookAtPlayer()
    {
        transform.LookAt(new Vector3(playerTransform.position.x, uiRectLife.transform.position.y, playerTransform.position.z));
    }

    public void DesactivateEnemy(float time)
    {
        Invoke("disableEnemy", time);
    }

    private void disableEnemy()
    {
        gameObject.SetActive(false);
    }
}
